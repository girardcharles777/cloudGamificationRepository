﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Timers;

using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions;
using PureCloudPlatform.Client.V2.Model;
using Microsoft.VisualBasic.CompilerServices;

namespace gamificationCloud
{
    public partial class GamificationService : ServiceBase
    {

        private int eventId = 1;
        private string encodedCode = String.Empty;
        private string loginURL = String.Empty;
        private string apiURL = String.Empty;
        private string channelURL = String.Empty;

        object ob;

        public GamificationService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
           /* if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";*/

        }

        internal void TestOnStartAndOnStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnPause();
            Console.ReadLine();
            this.OnContinue();
            Console.ReadLine();
            this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending (En attente de démarage).
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            Console.WriteLine("In onStart");

            // Set up a timer that triggers every minute.
            Timer timer = new Timer();
            timer.Interval = 60000; // 60 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();

            loginURL = "https://login.mypurecloud.de";
            apiURL = "https://api.mypurecloud.de";
            //WriteLogs("arguments : " + args[0]);

            
            CloudWorker(ob);

            // Update the service state to Running (à la fin de la méthode pour signaler le démarage).
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            Console.WriteLine("In OnStop");
        }

        protected override void OnContinue()
        {
            Console.WriteLine("In OnContinue");
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.
            //Partie log
            //eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            Console.WriteLine("Monitoring the System (fonction onTimer) : ", EventLogEntryType.Information, eventId++);
        }

        static public void CloudWorker(object ob)
        {
            Console.WriteLine("start CloudWorker -->");

            GamificationService gs = (GamificationService)ob;

            //Define the region
            PureCloudRegionHosts region = PureCloudRegionHosts.eu_central_1; // Genesys Cloud region
            Configuration.Default.ApiClient.setBasePath(region);

            // Configure OAuth2 access token for authorization: PureCloud OAuth
            // The following example is using the Client Credentials Grant
            var accessTokenInfo = Configuration.Default.ApiClient.PostToken("c49807eb-e1e0-403d-be3e-b8a4cc59d5be", "4GDp7fZzMhBxVYiewBs49Huy8MdgxQXbUQL2n7mCBAg");
            string token = accessTokenInfo.AccessToken;

            var list_users = RequestAPI.GET_Users("https://api.mypurecloud.de", token);


            Console.WriteLine("end CloudWorker -->");
        }

        //Partie pour l'état du service en attente
        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);
    }
}
