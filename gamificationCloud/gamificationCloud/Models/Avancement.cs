﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud.Models
{
    internal class Avancement
    {

        public int valeur;
        public int nombreThreshol;
        public string idAgent;
        public string nomSucces;

        public Avancement(int valeur,int nombreThreshol, string idAgent , string nomSucces)
        {
            this.valeur = valeur;
            this.nombreThreshol = nombreThreshol;
            this.idAgent = idAgent;
            this.nomSucces = nomSucces;
        }

    }
}
