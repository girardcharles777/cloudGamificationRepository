﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud.Models
{
    internal class AgentMetrics
    {
        public string idAgent;
        //Pour information, il existe deux classes metrics, une pour le JSON et qui correspond à l'identique au format JSON GENESYS
        //Une autre plus perso, avec des informations adapté aux besoins
        public List<MetricsConversation> ListMetricsConversation;

        public AgentMetrics(string idAgent, List<MetricsConversation> listMetricsConversation)
        {
            this.idAgent = idAgent;
            ListMetricsConversation = listMetricsConversation;
        }
    }
}
