﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud.Models
{
    internal class MetricsConversation
    {
        public string name;
        public int max;
        public int min;
        public int count;
        public int sum;
        public int moyenne;

        public MetricsConversation(string name, int max, int min, int count, int sum, int moyenne)
        {
            this.name = name;
            this.max = max;
            this.min = min;
            this.count = count;
            this.sum = sum;
            this.moyenne = moyenne;
        }
    }
}
