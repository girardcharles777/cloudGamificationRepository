﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud.Models
{
    internal class Succes
    {
        public string nonSucces;
        public List<Threshol> listThreshol;

        public Succes(string nonSucces, List<Threshol> listThreshol)
        {
            this.nonSucces = nonSucces;
            this.listThreshol = listThreshol;
        }
    }
}
