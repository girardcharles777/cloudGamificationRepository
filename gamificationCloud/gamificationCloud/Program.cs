﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud
{
    internal static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        static void Main(string[] args)
        {
            /*ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new GamificationService()
            };
            ServiceBase.Run(ServicesToRun);*/

            if (Environment.UserInteractive)
            {

                GamificationService serviceRequest = new GamificationService();
                serviceRequest.TestOnStartAndOnStop(args);

            }
            else // This block will execute when code is compiled as a Windows application
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new GamificationService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
