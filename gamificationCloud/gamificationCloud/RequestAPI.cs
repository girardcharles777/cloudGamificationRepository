﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace gamificationCloud
{
    public class RequestAPI
    {
        

        public static List<User> GET_Users(string api, string token)
        {
            bool completed = false;

            string url = api + "/api/v2/users";
            WebClient client = new WebClient();


            client.Headers.Add("cache-control", "no-cache");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            client.Headers.Add("Content-Type", "application/json");

            List<User> list_us = new List<User>();

            while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                UserClass us = JsonConvert.DeserializeObject<UserClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }
            return list_us;
        }


        //ATTENTION : j'ai l'impression de ne pas tout récup
        public static List<MetricsConversation> POST_conversationQuery(string api, string token, string interval)
        {
            List<MetricsConversation> conversationMetricsByUsers = new List<MetricsConversation>();


            string url = api + "/api/v2/analytics/conversations/aggregates/query";
            WebClient client = new WebClient();
            client.Headers.Add("cache-control", "no-cache");
            client.Headers.Add("Content-Type", "application/json");
            client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
            DateTime now = DateTime.Now.AddHours(-24);
            DateTime now2 = DateTime.Now;
            string body = "{" +
                            "\"interval\":  \"" + interval + "\"," +
                            "\"groupBy\": [ " +
                                "\"userId\"" +
                            "]," +
                            "\"filter\": {" +
                               "\"type\": \"and\"," +
                               "\"predicates\": [ " +
                                "{" +
                                    "\"type\": \"dimension\"," +
                                    "\"dimension\": \"mediaType\"," +
                                    "\"operator\": \"matches\"," +
                                    "\"value\": \"voice\"" +
                                "}" +
                            "]" +
                            "}," +
                            "\"views\": []," +
                            "\"metrics\": [ " +
                                "\"tAnswered\"," +
                                "\"tHandle\"," +
                                "\"tHeldComplete\"," +                               
                                "\"tTalkComplete\"" +
                            "]" +                          
                           "}";
            byte[] data = client.UploadData(url, Encoding.UTF8.GetBytes(body));
            string dataS = Encoding.UTF8.GetString(data); //Ligne pour le mettre sous un format JSON


            MetricsConversationClass conversationsQA = JsonConvert.DeserializeObject<MetricsConversationClass>(dataS);

            foreach(MetricsConversation conversation in conversationsQA.conversations)
            {
                conversationMetricsByUsers.Add(conversation);
            }
            return conversationMetricsByUsers;
        }


        public static List<Conversation> POST_GetConversation(string api, string token, string interval)
        {

            bool completed = false;

            int pageNumber = 1;
            List<Conversation> list_conversation = new List<Conversation>();

            string url = api + "/api/v2/analytics/conversations/details/query";

            while (!completed)
            {

                WebClient client = new WebClient();
                client.Headers.Add("cache-control", "no-cache");
                client.Headers.Add("Content-Type", "application/json");
                client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;


                string body = "{" +
                            "\"interval\": \"" + interval + "\"," +
                            "\"order\": \"asc\"," +
                            "\"orderBy\": \"conversationStart\"," +                          
                            "\"paging\": {" +
                                "\"pageSize\": 100," +
                                "\"pageNumber\":" + pageNumber +
                            "}," +
                             "\"segmentFilters\": [" +
                             "{" +
                               "\"type\": \"and\"," +
                               "\"predicates\": [ " +
                                "{" +
                                    "\"type\": \"dimension\"," +
                                    "\"dimension\": \"purpose\"," +
                                    "\"operator\": \"matches\"," +
                                    "\"value\": \"agent\"" +
                                "}" +
                            "]" +
                            "}" +
                            "]" +
                            "}";


                byte[] data = client.UploadData(url, Encoding.UTF8.GetBytes(body));
                string dataS = Encoding.UTF8.GetString(data);
                ConversationClass conv = JsonConvert.DeserializeObject<ConversationClass>(Encoding.UTF8.GetString(data));

                //Boucle qui verifie si il reste encore des conversations et sinon met stop la fonction while
                if (conv.Conversations != null)
                {
                    list_conversation.AddRange(conv.Conversations);
                    pageNumber += 1;
                }
                else
                {
                    completed = true;
                }
            }

           return list_conversation;
        }


        //REquette de test pour la gamification par userID -> qui fonctionne que si on a les droits
        public static void GET_dataGamification(string api, string token, string userID)
        {

            try
            {
                string url = api + "/api/v2/gamification/profiles/users/" + userID + "?workday=2022-11-09";
                WebClient client = new WebClient();


                client.Headers.Add("cache-control", "no-cache");
                client.Headers.Add("Content-Type", "application/json");
                client.Headers[HttpRequestHeader.Authorization] = "Bearer " + token;
                client.Encoding = Encoding.UTF8;

                string data = client.DownloadString(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);  
            }
            


           /* while (!completed)
            {
                //On récupère la réponse en format JSON
                string data = client.DownloadString(url);
                UserClass us = JsonConvert.DeserializeObject<UserClass>(data);

                list_us.AddRange(us.Entities);
                if (us.PageCount == us.PageNumber) completed = true;
                if (client.QueryString == null)
                    client.QueryString = new System.Collections.Specialized.NameValueCollection();
                else
                    client.QueryString.Clear();
                client.QueryString.Add("pageSize", "" + us.PageSize);
                us.PageNumber += 1;
                client.QueryString.Add("pageNumber", "" + us.PageNumber);

            }*/
        }



        //----------------------------------------Les classes------------------------------------------
        public class User
        {
            [JsonProperty(PropertyName = "id")]
            public string UserID { get; set; }
            [JsonProperty(PropertyName = "name")]
            public string Name { get; set; }
            [JsonProperty(PropertyName = "email")]
            public string Email { get; set; }
            public User(string _id, string _name, string _email)
            {
                UserID = _id;
                Name = _name;
                Email = _email;
            }
            public User()
            {
            }
        }

        public class UserClass
        {
            [JsonProperty(PropertyName = "entities")]
            public List<User> Entities { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public UserClass(List<User> _ent, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Entities = _ent;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public UserClass()
            {
            }
        }



        //Pour la requette conversation/details/query
        class ConversationClass
        {
            [JsonProperty(PropertyName = "conversations")]
            public List<Conversation> Conversations { get; set; }
            [JsonProperty(PropertyName = "pageSize")]
            public int PageSize { get; set; }
            [JsonProperty(PropertyName = "total")]
            public int Total { get; set; }
            [JsonProperty(PropertyName = "pageNumber")]
            public int PageNumber { get; set; }
            [JsonProperty(PropertyName = "pageCount")]
            public int PageCount { get; set; }
            public ConversationClass(List<Conversation> _conv, int _pagesize, int _total, int _pagenumber, int _pagecount)
            {
                Conversations = _conv;
                PageSize = _pagesize;
                Total = _total;
                PageNumber = _pagenumber;
                PageCount = _pagecount;
            }
            public ConversationClass()
            {
            }
        }

        public class Conversation
        {
            [JsonProperty(PropertyName = "conversationId")]
            public string ConversationID { get; set; }
            [JsonProperty(PropertyName = "conversationStart")]
            public DateTime ConversationStart { get; set; }
            [JsonProperty(PropertyName = "conversationEnd")]
            public DateTime ConversationEnd { get; set; }
            [JsonProperty(PropertyName = "originatingDirection")]
            public string OriginatingDirection { get; set; }
            [JsonProperty(PropertyName = "participants")]
            public List<Participant> participants { get; set; }

            public Conversation(string _id, DateTime _start, DateTime _end, string _direction, List<Participant> _participantId)
            {
                ConversationID = _id;
                ConversationStart = _start;
                ConversationEnd = _end;
                OriginatingDirection = _direction;
                participants = _participantId;
            }
            public Conversation()
            {
            }
        }

        public class Participant
        {
            [JsonProperty(PropertyName = "participantId")]
            public string ParticipantID { get; set; }
            [JsonProperty(PropertyName = "participantName")]
            public string ParticipantName { get; set; }
            [JsonProperty(PropertyName = "purpose")]
            public string Purpose { get; set; }
            [JsonProperty(PropertyName = "userId")]
            public string UserId { get; set; }
            

            public Participant(string _participantId, string _participantName, string _purp, string _userId)
            {
                ParticipantID = _participantId;
                ParticipantName = _participantName;
                Purpose = _purp;
                UserId = _userId;
               
            }
            public Participant()
            {
            }
        }



        //Pour l'url conversation/agregatte/query
        public class MetricsConversationClass
        {
            [JsonProperty(PropertyName = "results")]
            public List<MetricsConversation> conversations { get; set; }
            
            public MetricsConversationClass(List<MetricsConversation> _conversations)
            {
                conversations = _conversations;
                
            }
            public MetricsConversationClass()
            {
            }
        }

        public class MetricsConversation
        {
            [JsonProperty(PropertyName = "group")]
            public Group groups { get; set; }
            [JsonProperty(PropertyName = "data")]
            public List<Data> datas { get; set; }

            public MetricsConversation(Group _groups, List<Data> _datas)
            {
                groups = _groups;
                datas = _datas;

            }
            public MetricsConversation()
            {
            }
        }
    }

    public class Group
    {
        [JsonProperty(PropertyName = "userId")]
        public string userId { get; set; }

        public Group(string _userId)
        {
            userId = _userId;

        }
        public Group()
        {
        }
    }

    public class Data
    {
        [JsonProperty(PropertyName = "metrics")]
        public List<Metrics> mertrics { get; set; }
        

        public Data(List<Metrics> _mertrics)
        {
            mertrics = _mertrics;

        }
        public Data()
        {
        }
    }

    public class Metrics
    {
        [JsonProperty(PropertyName = "metric")]
        public String mertricName { get; set; }

        [JsonProperty(PropertyName = "stats")]
        public Stats stats { get; set; }


        public Metrics(String _metricsName, Stats _stats)
        {
            mertricName = _metricsName; 
            stats = _stats;

        }
        public Metrics()
        {
        }
    }

    public class Stats
    {
        [JsonProperty(PropertyName = "max")]
        public int max { get; set; }
        [JsonProperty(PropertyName = "min")]
        public int min { get; set; }
        [JsonProperty(PropertyName = "count")]
        public int count { get; set; }
        [JsonProperty(PropertyName = "sum")]
        public int sum { get; set; }


        public Stats(int _max, int _min, int _count, int _sum)
        {
            max = _max;
            min = _min;
            count = _count;
            sum = _sum;

        }
        public Stats()
        {
        }
    }


}
