﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using gamificationCloud.Models;
using MySql.Data.MySqlClient;
using static gamificationCloud.RequestAPI;

namespace gamificationCloud
{
    internal class Bdd
    {
        //attributs
        public MySqlConnection connection;

        // Constructeur
        public Bdd()
        {
            this.InitConnexion();
        }

        // Méthode pour initialiser la connexion
        public void InitConnexion()
        {

            string server = "localhost";
            string database = "cloud_gamification";
            string user = "root";
            string password = "";
            string port = "3308";
            string sslM = "none";

            string connString = String.Format("server={0};port={1};user id={2}; password={3}; database={4}; SslMode={5}", server, port, user, password, database, sslM);

            this.connection = new MySqlConnection(connString);
            try
            {
                this.connection.Open();

                Console.WriteLine("Connection BDD Successful");

                this.connection.Close();
            }
            catch (MySqlException e)
            {
                Console.WriteLine(e.Message + connString);
            }
        }


        //requette qui insert un nouveau agent
        public void addAgent(User user)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL               
                requette.CommandText = "INSERT INTO agent(idAgent,nom,email) VALUES (@agentId,@nom,@email)";

                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@agentId", user.UserID);
                requette.Parameters.AddWithValue("@nom", user.Name);
                requette.Parameters.AddWithValue("@email", user.Email);
          

                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette INSERT user vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }


        public bool agentPresent(string idAgent)
        {
            try
            {

                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL
                requette.CommandText = "SELECT COUNT(*) FROM agent WHERE idAgent = @idAgent";

                requette.Parameters.AddWithValue("@idAgent", idAgent);

                // Exécution de la commande SQL
                var reponse = int.Parse(requette.ExecuteScalar().ToString()); //Attention executeScalar permet de renvoyer une seul ligne de la réponse.

                // Fermeture de la connexion
                this.connection.Close();

                return reponse > 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette agentPresent vers la base de données :\n" + ex.ToString());
                this.connection.Close();
                return false;
            }
        }

        //Permet de savoir si un avancement est déjà présent
        public bool avancementPresent(string idAgent, string nomSucces)
        {
            try
            {

                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL
                requette.CommandText = "SELECT COUNT(*) FROM avancement WHERE idAgent = @idAgent and nomSucces = @idSucces";

                requette.Parameters.AddWithValue("@idAgent", idAgent);
                requette.Parameters.AddWithValue("@idSucces", nomSucces);


                // Exécution de la commande SQL
                var reponse = int.Parse(requette.ExecuteScalar().ToString()); //Attention executeScalar permet de renvoyer une seul ligne de la réponse.

                // Fermeture de la connexion
                this.connection.Close();

                return reponse > 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette avancement present vers la base de données :\n" + ex.ToString());
                this.connection.Close();
                return false;
            }
        }
        
        //requette qui insert un nouveau avancement
        public void addAvancement(int valeur, string idAgent, string nomSucces)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL               
                requette.CommandText = "INSERT INTO avancement(valeurAvancement,numeroThreshol,idAgent,nomSucces) VALUES (@valeur,0,@idAgent,@idSucces)";

                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@valeur", valeur);
                requette.Parameters.AddWithValue("@idAgent", idAgent);
                requette.Parameters.AddWithValue("@idSucces", nomSucces);


                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette INSERT avancement vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }

        //requette qui met a jour la valeur d'un avancement pour un agentId et un succesId donnés 
        public void updateavancement(int valeur, string idAgent, string nomSucces)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL               
                requette.CommandText = "update avancement set valeurAvancement = valeurAvancement + @valeur where idAgent = @idAgent and nomSucces=@idSucces";

                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@valeur", valeur);
                requette.Parameters.AddWithValue("@idAgent", idAgent);
                requette.Parameters.AddWithValue("@idSucces", nomSucces);


                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette update avancement vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }



        //requette qui insert une nouvelle statistique
        public void addStatistique(string nomStat, DateTime dateDebut, DateTime dateFin, int valeur, string idAgent)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                //Transformation des datesTime en date : 
                DateTime dateDebutWithoutTime = dateDebut.Date;
                DateTime dateFinWithoutTime = dateFin.Date;

                // Requête SQL               
                requette.CommandText = "INSERT INTO Statistique(nomStat,dateStatDebut,dateStatFin,valeurStat,idAgent) VALUES (@nom,@dateDebut,@dateFin,@valeur, @idAgent)";
                
                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@nom", nomStat);
                requette.Parameters.AddWithValue("@dateDebut", dateDebutWithoutTime);
                requette.Parameters.AddWithValue("@dateFin", dateFinWithoutTime);
                requette.Parameters.AddWithValue("@valeur", valeur);
                requette.Parameters.AddWithValue("@idAgent", idAgent);




                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette add statistique vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }



        //requette qui recupère tous les avancements
        public List<Avancement> selectAvancement()
        {
            try
            {
                List<Avancement> listAvancement = new List<Avancement>();

                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                string sql = "SELECT * FROM avancement";
                MySqlCommand cmd = new MySqlCommand(sql, this.connection);
                MySqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    int valeur = rdr.GetInt32(1);
                    int nombreThreshol = rdr.GetInt32(2);
                    string idAgent = rdr.GetString(3);
                    string nomSucces = rdr.GetString(4);
                    Avancement avancement = new Avancement(valeur, nombreThreshol, idAgent, nomSucces);
                    listAvancement.Add(avancement);
                }
                rdr.Close();

                // Fermeture de la connexion
                this.connection.Close();

                return listAvancement;
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette SElect avancement vers la base de données :\n" + ex.ToString());
                this.connection.Close();
                return null;
            }
        }


        //requette qui récupère tous les succès et leurs threshols
        public List<Succes> selectSuccesAndThreshol()
        {
            try
            {
                List<Succes> listSucces = new List<Succes>();

                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                string sql = "SELECT * FROM threshol ORDER BY threshol.nomSucces, threshol.threshol ASC";
                MySqlCommand cmd = new MySqlCommand(sql, this.connection);
                MySqlDataReader rdr = cmd.ExecuteReader();

                string ancienNomSucces = "demarage";
               

                while (rdr.Read())
                {
                    string nomSucces= rdr.GetString(3);
                    int valThreshol = rdr.GetInt32(1);
                    int trophee = rdr.GetInt32(2);

                    if (nomSucces != ancienNomSucces)
                    {
                        List<Threshol> listThreshol = new List<Threshol>();
                        Threshol threshol = new Threshol(valThreshol, trophee);
                        listThreshol.Add(threshol);

                        Succes succes = new Succes(nomSucces, listThreshol);
                        listSucces.Add(succes);
                       
                    }
                    else
                    {
                     
                        Threshol threshol = new Threshol(valThreshol, trophee);
                        listSucces[listSucces.Count - 1].listThreshol.Add(threshol);
                    }
                    ancienNomSucces = nomSucces;

                }
                rdr.Close();

                // Fermeture de la connexion
                this.connection.Close();

                return listSucces;
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette select succes et threshol vers la base de données :\n" + ex.ToString());
                this.connection.Close();
                return null;
            }
        }


        //requette qui insert un nouveau badge
        public void insertBadge(DateTime dateBadge, int trophee, int threshol, string idAgent, string nomSucces)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();


                // Requête SQL               
                requette.CommandText = "INSERT INTO badge(dateBadge,trophee,threshol,idAgent,nomSucces) VALUES (@dateBadge,@trophee,@threshol,@idAgent,@nomSucces)";

                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@dateBadge", dateBadge);
                requette.Parameters.AddWithValue("@trophee", trophee);
                requette.Parameters.AddWithValue("@threshol", threshol);
                requette.Parameters.AddWithValue("@idAgent", idAgent);
                requette.Parameters.AddWithValue("@nomSucces", nomSucces);




                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette INSERT avancement vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }


        //requette qui met a jour la valeur d'un avancement pour un agentId et un succesId donnés et qui ajoute 1  
        public void updateAvancementNombreThrehol(string idAgent, string nomSucces)
        {
            try
            {
                //Ouvrir la connexion avec la base de données 
                this.connection.Open();

                // Création d'une commande MySQL en fonction de l'objet connection
                MySqlCommand requette = this.connection.CreateCommand();

                // Requête SQL               
                requette.CommandText = "update avancement set numeroThreshol = numeroThreshol + 1 where idAgent = @idAgent and nomSucces=@idSucces";

                // utilisation de l'objet contact passé en paramètre
                requette.Parameters.AddWithValue("@idAgent", idAgent);
                requette.Parameters.AddWithValue("@idSucces", nomSucces);


                // Exécution de la commande SQL
                requette.ExecuteNonQuery();

                // Fermeture de la connexion
                this.connection.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine("erreur lors de la requette update avancement vers la base de données :\n" + ex.ToString());
                this.connection.Close();
            }
        }



    }
}
