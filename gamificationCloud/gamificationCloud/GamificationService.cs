﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Timers;

using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Extensions;
using PureCloudPlatform.Client.V2.Model;
using Microsoft.VisualBasic.CompilerServices;
using gamificationCloud.Models;
using System.Configuration;

namespace gamificationCloud
{
    public partial class GamificationService : ServiceBase
    {

        private int eventId = 1;

        object ob;

        public GamificationService()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
           /* if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";*/

        }

        internal void TestOnStartAndOnStop(string[] args)
        {
            this.OnStart(args);
            Console.ReadLine();
            this.OnPause();
            Console.ReadLine();
            this.OnContinue();
            Console.ReadLine();
            this.OnStop();
        }

        protected override void OnStart(string[] args)
        {
            // Update the service state to Start Pending (En attente de démarage).
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            Console.WriteLine("In onStart");

            // Set up a timer that triggers every minute.
            Timer timer = new Timer();
            timer.Interval = 60000; // 60 seconds
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();

            //WriteLogs("arguments : " + args[0]);

            
            CloudWorker(ob);

            // Update the service state to Running (à la fin de la méthode pour signaler le démarage).
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        protected override void OnStop()
        {
            Console.WriteLine("In OnStop");
        }

        protected override void OnContinue()
        {
            Console.WriteLine("In OnContinue");
        }


        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.
            //Partie log
            //eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, eventId++);
            Console.WriteLine("Monitoring the System (fonction onTimer) : ", EventLogEntryType.Information, eventId++);
        }


        static public void CloudWorker(object ob)
        {

            //Get crediential : 
            string clientID = ConfigurationManager.AppSettings["ClientID"];
            string clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            string API_URL = ConfigurationManager.AppSettings["API_URL"];

            //Déclaration des variables : 
            GamificationService gs = (GamificationService)ob;
            Bdd dbConnexion = new Bdd();


            Console.WriteLine("Start CloudWorker -->");
           

            //Define the region
            PureCloudRegionHosts region = PureCloudRegionHosts.eu_central_1; // Genesys Cloud region
            PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.setBasePath(region);



            // Configure OAuth2 access token for authorization: PureCloud OAuth
            // The following example is using the Client Credentials Grant
            var accessTokenInfo = PureCloudPlatform.Client.V2.Client.Configuration.Default.ApiClient.PostToken(clientID, clientSecret);
            string token = accessTokenInfo.AccessToken;



            //--------------------------------------RECUPERATION DES USERS------------------------------------------------------------------------------

            Console.Write("Récupération des users, status : ");
            List<RequestAPI.User> list_users = new List<RequestAPI.User>();

            try
            {
                //Récupération des users :
                list_users = RequestAPI.GET_Users(API_URL, token);
                Console.WriteLine("OK");
                Console.WriteLine();

                Console.Write("Ajout des users dans la base de données, status : ");

                //Ajout des users dans la base de données
                foreach (RequestAPI.User user in list_users)
                {
                    if (!dbConnexion.agentPresent(user.UserID))
                    {
                        dbConnexion.addAgent(user);
                    }
                }

                Console.WriteLine("OK");
                Console.WriteLine();

            }
            catch (Exception ex)
            {
                Console.WriteLine("KO : " + ex.Message);
            }





            //--------------------------------------RECUPERATION DES CONVERSATIONS------------------------------------------------------------------------------

            Console.Write("Récupération des conversations pour les dernière 24h, status : ");

            //Récupération de toute les conversations en fonction d'un intervalle
            //récupération des dates : auourd'hui et hier (24h de moins et 1 minutes au cas ou mais attention de pas recupérer des conv de la journée d'hier)

            DateTime thisDay = DateTime.Now;

            string thisDayString = thisDay.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            DateTime yesteday = thisDay.AddDays(-1);
            DateTime yestedayLess10Min = yesteday.AddMinutes(-1);
            string yestedayStringLess10Min = yestedayLess10Min.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            // string intervalDateManuelle = "2023-01-03T00:00:01.000Z/2023-01-03T23:59:59.000Z";

            string interval = yestedayStringLess10Min + "/" + thisDayString;
            List<RequestAPI.Conversation> listeConversation = new List<RequestAPI.Conversation>();

            try
            {
                listeConversation = RequestAPI.POST_GetConversation(API_URL, token, interval);
                Console.WriteLine("OK");

            }
            catch (Exception ex)
            {
                Console.WriteLine("KO : " + ex.Message);
            }

            //Une fois les conversations récupérées pour les dernières 24h, on va attribuer le nombre d'appel pour chaque idAgent 
            //On commence par créer un dictonaire agent et nbrAppele et on y met tous les agentID
            Dictionary<string, int> dictonaryAgentNbrAppelleToDay = new Dictionary<string, int>();

            foreach (RequestAPI.User user in list_users)
            {
                dictonaryAgentNbrAppelleToDay.Add(user.UserID, 0);
            }

            //Pour chaque conversation, 
            foreach (RequestAPI.Conversation conversation in listeConversation)
            {
                //Il y a plusieurs Participant (4)
                foreach (RequestAPI.Participant participants in conversation.participants)
                {
                    if (participants.Purpose.Contains("agent"))
                    {
                        //On incremente de 1 la valeur du nbr appel pour l'agent
                        if (dictonaryAgentNbrAppelleToDay.ContainsKey(participants.UserId))
                        {
                            int nbrAppel;
                            dictonaryAgentNbrAppelleToDay.TryGetValue(participants.UserId, out nbrAppel);
                            dictonaryAgentNbrAppelleToDay[participants.UserId] = nbrAppel += 1;
                        }
                    }
                }

            }


            Console.Write("Ajout du nombre de conversations par userID dans la table avancement et statistiques, status : ");

            try
            {
                string nomStat = "nombreAppel";

                //Pour toutes les valeurs du du Dictionnaire, on ajout dans la BDD
                foreach (KeyValuePair<string, int> pair in dictonaryAgentNbrAppelleToDay)
                {

                    updateOrInsertAvancement(pair.Key, nomStat, pair.Value, dbConnexion);

                    if (pair.Value > 0)
                    {
                        dbConnexion.addStatistique(nomStat, yestedayLess10Min, thisDay, pair.Value, pair.Key);

                    }
                }
                Console.WriteLine("OK");
            }
            catch (Exception ex)
            {
                Console.WriteLine("KO : " + ex.Message);
            }






            //--------------------------------------RECUPERATION DES METRICS------------------------------------------------------------------------------

            Console.Write("Recupération des Metrics (stastiques); Status : ");

            List<RequestAPI.MetricsConversation> conversationMetricsByUsers = new List<RequestAPI.MetricsConversation>();

            //Récupération des metrics conversation aggregate query
            try
            {
                conversationMetricsByUsers = RequestAPI.POST_conversationQuery(API_URL, token, interval);
                Console.WriteLine("OK");
            }
            catch (Exception ex)
            {
                Console.WriteLine("KO");
                Console.WriteLine(ex.ToString());
            }

            //Pour toutes les metrics par userID récupérées, on va les ajouter à une liste de AgentMetrics (classe plus peronnels)
            //On est pas obligé de passé par cette étape mais c'est beaucoup plus propre, en effet on aurait directement pus ajouter dans la base depuis la conversationMetricsByUsers
            List<Models.AgentMetrics> listAgentMetrics = new List<Models.AgentMetrics>();

            //Pour tous les resultats :
            foreach (RequestAPI.MetricsConversation metricsConversation in conversationMetricsByUsers)
            {
                List<Models.MetricsConversation> listMetricsConversation = new List<Models.MetricsConversation>();
                //Pour toutes les metrics de l'agent ( soit group soit data dans le JSON) : 
                foreach (var data in metricsConversation.datas)
                {
                    foreach (var metrics in data.mertrics) //
                    {
                        int moyenne = metrics.stats.sum / metrics.stats.count;
                        Models.MetricsConversation metricsForThisAgentID = new Models.MetricsConversation(metrics.mertricName, metrics.stats.max, metrics.stats.min, metrics.stats.count, metrics.stats.sum, moyenne);
                        listMetricsConversation.Add(metricsForThisAgentID);
                    }
                }
                Models.AgentMetrics AgentAndMetrics = new Models.AgentMetrics(metricsConversation.groups.userId, listMetricsConversation);
                listAgentMetrics.Add(AgentAndMetrics);
            }


            //---------------Intégration des metrics par utilisateurs dans la base de données (Avancement et stats) :

            Console.Write("(BDD) -> ajout des metrics dans les tables staistique et avancement; Status : ");

            try
            {
                foreach (Models.AgentMetrics agentMetrics in listAgentMetrics)
                {
                    foreach (Models.MetricsConversation metricsConversation in agentMetrics.ListMetricsConversation)
                    {
                        int moyenne = metricsConversation.sum / metricsConversation.count;
                        switch (metricsConversation.name)
                        {
                            case "tAnswered":

                                dbConnexion.addStatistique(metricsConversation.name + "_Max", yestedayLess10Min, thisDay, metricsConversation.max, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_Min", yestedayLess10Min, thisDay, metricsConversation.min, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_sum", yestedayLess10Min, thisDay, metricsConversation.sum, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_Moyenne", yestedayLess10Min, thisDay, moyenne, agentMetrics.idAgent);

                                updateOrInsertAvancement(agentMetrics.idAgent, "averageTimeAnswered", moyenne, dbConnexion);
                                break;

                            case "tTalkComplete":

                                dbConnexion.addStatistique(metricsConversation.name + "_Max", yestedayLess10Min, thisDay, metricsConversation.max, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_Min", yestedayLess10Min, thisDay, metricsConversation.min, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_sum", yestedayLess10Min, thisDay, metricsConversation.sum, agentMetrics.idAgent);
                                dbConnexion.addStatistique(metricsConversation.name + "_Moyenne", yestedayLess10Min, thisDay, moyenne, agentMetrics.idAgent);

                                updateOrInsertAvancement(agentMetrics.idAgent, "timeTalk", metricsConversation.sum, dbConnexion);
                                break;

                            case "tHeldComplete":

                                dbConnexion.addStatistique(metricsConversation.name + "_Moyenne", yestedayLess10Min, thisDay, moyenne, agentMetrics.idAgent);
                                break;

                            case "tHandle":

                                dbConnexion.addStatistique(metricsConversation.name + "_sum", yestedayLess10Min, thisDay, metricsConversation.sum, agentMetrics.idAgent);
                                updateOrInsertAvancement(agentMetrics.idAgent, "timeWork", metricsConversation.sum, dbConnexion);

                                break;

                            default:
                                break;
                        }
                    }
                }
                Console.WriteLine("OK");

            }
            catch (Exception e)
            {
                Console.WriteLine("KO : ");
                Console.WriteLine(e.ToString());

            }

            //------------------------------------------------------------partie Creation badge------------------------------------------------

            //cette partie permet lorsque la valeur d'un avancement atteint le threshol d'un succès, on ajoute un à la valeur de l'avancement
            //et on créer un nouveau badge.
            Console.Write("Ajout des nouveaux badge; Status : ");

            try
            {
                //Première étape, il faut récupérer puis parcourir tous les avancements de la BD:
                List<Avancement> listAvancement = new List<Avancement>();
                listAvancement = dbConnexion.selectAvancement();

                //On récupère aussi un listes des succes associés d'un liste de leurs threshol : 
                List<Succes> listSucces = new List<Succes>();
                listSucces = dbConnexion.selectSuccesAndThreshol();

                Succes succesAComparer = new Succes(null,null);
                //On parcourt les avancements : 
                foreach (Avancement avancement in listAvancement)
                {
                    float val = 0;

                    //On est obligé de recup le succè à comparer
                    foreach (Succes succes in listSucces)
                    {
                        if (succes.nonSucces == avancement.nomSucces)
                        {
                            succesAComparer = succes;
                            break;
                        }
                    }

                    //En fonction du succe, la valeur a comparer est différente ( h, min, nombre...)
                    switch (avancement.nomSucces)
                    {
                        case "nombreAppel":
                            //en nombre donc on touche pas 
                            val = avancement.valeur;
                            if (succesAComparer.listThreshol[avancement.nombreThreshol].threshol <= val)
                            {
                                //Alors on créer un badge, sans oublier d'ajouter 1 à la valeurThreshol de l'avancement 
                                DateTime toDay = DateTime.Now;
                                dbConnexion.insertBadge(toDay, succesAComparer.listThreshol[avancement.nombreThreshol].trophee, succesAComparer.listThreshol[avancement.nombreThreshol].threshol, avancement.idAgent, avancement.nomSucces);
                                dbConnexion.updateAvancementNombreThrehol(avancement.idAgent, avancement.nomSucces);
                            }

                            break;

                        case "timeWork":
                            //En h
                            val = avancement.valeur / 3600000;
                            if (succesAComparer.listThreshol[avancement.nombreThreshol].threshol <= val)
                            {
                                //Alors on créer un badge, sans oublier d'ajouter 1 à la valeurThreshol de l'avancement 
                                DateTime toDay = DateTime.Now;
                                dbConnexion.insertBadge(toDay, succesAComparer.listThreshol[avancement.nombreThreshol].trophee, succesAComparer.listThreshol[avancement.nombreThreshol].threshol, avancement.idAgent, avancement.nomSucces);
                                dbConnexion.updateAvancementNombreThrehol(avancement.idAgent, avancement.nomSucces);
                            }

                            break;

                        case "averageTimeAnswered": //diff des autres
                            val = avancement.valeur / 1000;
                            int index = 0;
                            //Attention on récupère les valeur du plus petit au plus grand donc 45,50,55... il faut par ordre changer car le primier succès à valider est bien le plus grand (moyenne de temps de réponse doit etre de + en + petit)
                            switch (avancement.nombreThreshol)
                            {
                                case 0:
                                    index = 2;
                                    break;
                                case 1:
                                    index = 1;
                                    break;
                                case 2:
                                    index = 0;
                                    break;
                               
                            }
                            if (succesAComparer.listThreshol[index].threshol >= val)
                            {
                                //Alors on créer un badge, sans oublier d'ajouter 1 à la valeurThreshol de l'avancement 
                                DateTime toDay = DateTime.Now;
                              
                                dbConnexion.insertBadge(toDay, succesAComparer.listThreshol[index].trophee, succesAComparer.listThreshol[avancement.nombreThreshol].threshol, avancement.idAgent, avancement.nomSucces);
                                dbConnexion.updateAvancementNombreThrehol(avancement.idAgent, avancement.nomSucces);
                            }

                            break;

                        case "timeTalk":
                            //En h
                            val = avancement.valeur / 3600000;
                            if (succesAComparer.listThreshol[avancement.nombreThreshol].threshol <= val)
                            {
                                //Alors on créer un badge, sans oublier d'ajouter 1 à la valeurThreshol de l'avancement 
                                DateTime toDay = DateTime.Now;
                                dbConnexion.insertBadge(toDay, succesAComparer.listThreshol[avancement.nombreThreshol].trophee, succesAComparer.listThreshol[avancement.nombreThreshol].threshol, avancement.idAgent, avancement.nomSucces);
                                dbConnexion.updateAvancementNombreThrehol(avancement.idAgent, avancement.nomSucces);
                            }

                            break;

                        default:
                            break;
                    }

                  

                }
            


                Console.WriteLine("OK");
            }
            catch(Exception e)
            {
                Console.WriteLine("KO : ");
                Console.WriteLine(e.ToString());
            }



            Console.WriteLine("end CloudWorker -->");

        }


//-------------------------------------------------------------------MES FONCTIONS--------------------------------------------------------


        static void updateOrInsertAvancement(string idAgent, string nomSucces, int valeur, Bdd dbConnexion)
        {
            if (dbConnexion.avancementPresent(idAgent, nomSucces))
            {
                dbConnexion.updateavancement(valeur, idAgent, nomSucces);
            }
            else
            {
                dbConnexion.addAvancement(valeur, idAgent, nomSucces);
            }
        }


        //-------------------------------------------------------------------L'état du service--------------------------------------------------------


        //Partie pour l'état du service en attente
        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public int dwServiceType;
            public ServiceState dwCurrentState;
            public int dwControlsAccepted;
            public int dwWin32ExitCode;
            public int dwServiceSpecificExitCode;
            public int dwCheckPoint;
            public int dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(System.IntPtr handle, ref ServiceStatus serviceStatus);
    }
}
